# -*- coding: utf-8 -*-
# -*- coding: utf-8 -*-
"""
***二维码***

Created on xxx xxx x xx:xx:xx xxxx

@author: xxx
"""


import socket
import re

def get_QR_num(ip, port, qrcode):
    ''' 扫码数获取 '''
    '''
    输入说明：
        ip：二维码ip地址
        port：二维码端口号
        qrcode：二维码所指代的字符串
    返回值：
        扫码数
    '''

    tcp_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    tcp_socket.connect((ip, port))
    tcp_socket.send((qrcode+'\n').encode("utf-8"))
    recv_data = tcp_socket.recv(1024)
    num = recv_data.decode("utf-8")
    num = int(num)

    return num


if __name__ == '__main__':
    # 测试用代码
    ip_QR = "49.232.193.125" # 二维码ip地址
    port_QR = 2808 # 二维码端口号
    qrcode = 'a'
    print(get_QR_num(ip_QR, port_QR, qrcode))
