# -*- coding: utf-8 -*-
# -*- coding: utf-8 -*-
"""
***扫码生产***

Created on xxx xxx x xx:xx:xx xxxx

@author: xxx

"""

import os
import time
import tensorflow as tf
from keras.backend.tensorflow_backend import set_session

from QRcode import get_QR_num
from modbus import modbus_write, modbus_read
from counter import CountManModel
from push import push_order

def do_produce(master, ADR, num, order):
    ''' 生产过程 '''
    '''
    输入说明：
        master：modbus主机
        ADR：生产机器地址码
        num：生产数量
    '''
    HD100 = 41188 # 生产数量寄存器
    HD102 = 41190 # 扫码数量寄存器
    HD112 = 41200 # 生产状态寄存器
    modbus_write(master, ADR, HD102, order) # 扫码数量写入
    if num <= 0: # 无需生产
        return
    modbus_write(master, ADR, HD100, num) # 进行生产
    done_produce = 0
    while done_produce == 0: # 确认状态，生产完成后进入下一步，未完成时不进行下一次扫码读取
        info = modbus_read(master, ADR, HD112, 1)
        done_produce = info[0]
        time.sleep(0.5)
    modbus_write(master, ADR, HD112, 2) # 告知生产机器本机已知晓生产完成

def produce_baozi_process(master, ADR, ip_QR, port_QR):
    ''' 扫码生产进程 '''
    '''
    输入说明：
        master：modbus主机
        ADR：生产机器地址码
        ip_QR：二维码ip地址
        port_QR：二维码端口号
    '''

    config = tf.ConfigProto()
    config.gpu_options.allow_growth=True
    set_session(tf.Session(config=config))

    COUNT_MAN_MODEL = CountManModel() # 人群计数模型

    ahead_num_last = 0 # 预产量剩余
    while True:
        qrcode_file = '/tmp/baozi_qrcode'
        if os.path.exists(qrcode_file):
            os.system('rm ' + qrcode_file)
        print('等待扫码：', end='')
        while True:
            try:
                with open(qrcode_file) as f:
                    qrcode = f.read()
                print(qrcode)

            except: 
                time.sleep(0.1)
                continue

            break
                
        print("制作中...")
        order_num = get_QR_num(ip_QR, port_QR, qrcode) # 扫码数获取
        current_man_num = COUNT_MAN_MODEL.current_count() # 人数获取

        ahead_num = 0 # 预产量计算，此部分可能仍需要修改
        # TODO
        if current_man_num < 4:
            ahead_num = 0
        elif current_man_num < 16:
            ahead_num = int(current_man_num*0.6) - 1
        else:
            ahead_num = 6

        produce_num = max(0, order_num + ahead_num - ahead_num_last) # 生产数量计算

        if produce_num > 0: # 进行生产
            do_produce(master, ADR, produce_num, order_num)

        push_order(order_num) # 推出包子

        ahead_num_last = max(0, ahead_num) # 更新预产量剩余
