# -*- coding: utf-8 -*-
# -*- coding: utf-8 -*-
"""
***人群计数***

Created on xxx xxx x xx:xx:xx xxxx

@author: xxx
"""


import cv2
import numpy as np
import tensorflow as tf
from PIL import Image
from tensorflow.keras.models import model_from_json


def create_img(img):
    image = Image.fromarray(cv2.cvtColor(img, cv2.COLOR_BGR2RGB))
    im = image.convert('RGB')
    im = im.resize((400, 300), Image.ANTIALIAS)
    im = np.array(im)
    im = im / 255.0
    im[:, :, 0] = (im[:, :, 0] - 0.485) / 0.229
    im[:, :, 1] = (im[:, :, 1] - 0.456) / 0.224
    im[:, :, 2] = (im[:, :, 2] - 0.406) / 0.225
    im = np.expand_dims(im, axis=0)
    return im

def load_model():
    with open('models/Model.json', 'r') as json_file:
        loaded_model_json = json_file.read()
    loaded_model = model_from_json(loaded_model_json)
    loaded_model.load_weights("weights/model_B_weights.h5")
    return loaded_model



class CountManModel():
    ''' 人群计数模型 '''
    def __init__(self):
        self.model = load_model()


    def current_count(self):
        ''' 人数获取 '''
        cap = cv2.VideoCapture(0)
        ret, frame = cap.read()
        if ret == False:
            raise Exception("人数图像读取失败")

        image = create_img(frame)
        ans = self.model.predict(image)
        return int(np.sum(ans))
