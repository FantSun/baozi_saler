# -*- coding: utf-8 -*-
# -*- coding: utf-8 -*-
"""
***库存检查***

Created on xxx xxx x xx:xx:xx xxxx

@author: xxx
"""

import time

from modbus import modbus_read
from message import send_SMI

def get_store(master, ADR):
    ''' 获取库存量 '''
    '''
    输入说明：
        master：modbus主机
        ADR：生产机器地址码
    返回值：
        剩余包子组数
    '''

    HD106 = 41194 # 库存状态寄存器起始地址
    num_HD = 6 # 库存状态寄存器数量
    info = modbus_read(master, ADR, HD106, num_HD) # 读取库存状态寄存器状态
    return sum(info)

def check_store_process(master, ADR):
    ''' 库存检查进程 '''
    '''
    输入说明：
        master：modbus主机
        ADR：生产机器地址码
    '''
    WARNING_STAGE = 0 # 历史库存状态码，充足为0，不足为1，严重不足为2，用以避免重复发送短信
    while True:
        num_baozi = get_store(master, ADR) # 获取库存量

        if num_baozi <= 2:
            if WARNING_STAGE < 1: # 库存不足，发送短信
                send_SMI("包子剩余组数：" + str(num_baozi) + "，请及时补充。")
                WARNING_STAGE = 1
        elif num_baozi <= 1:
            if WARNING_STAGE < 2: # 库存严重不足，发送短信
                send_SMI("包子剩余组数：" + str(num_baozi) + "，请立即补充。")
                WARNING_STAGE = 2
        else:
            WARNING_STAGE = 0

        time.sleep(5)
