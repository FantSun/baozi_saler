# -*- coding: utf-8 -*-
# -*- coding: utf-8 -*-
"""
***主进程***

Created on xxx xxx x xx:xx:xx xxxx

@author: xxx

"""


import os
import serial
import threading
from modbus_tk import modbus_rtu

from progress_produce import produce_baozi_process
from progress_check import check_store_process
from QR_scanner import QR_scan_loop

if __name__ == '__main__':

    ADR = 1                  # 生产机器地址码
    ip_QR = "49.232.193.125" # 二维码ip地址
    port_QR = 2808           # 二维码端口号
    port_PC = '/dev/ttyTHS1' # 本机端口号
    scanner_name = 'HIDKeyBoard' # 扫码枪设备名（部分名即可，保证可被唯一区分）
    os.environ['CUDA_VISIBLE_DEVICES'] = '0'

    print("主进程号", os.getpid())
    with open("pid_main.log", "w") as f: # 该文件中存储进程号，方便进行监控管理 
        f.write(str(os.getpid()))

    master = modbus_rtu.RtuMaster( # modbus主机
        serial.Serial(port=port_PC, baudrate=19200, bytesize=8, stopbits=1, xonxoff=0, parity='E') # 实机调试parity='E'，usb转换模拟调试parity='N'
    )
    master.set_timeout(5)
    master.set_verbose(True)

    # 库存检查进程
    t_check = threading.Thread(
        target=check_store_process,
        args=[master, ADR]
    ) 
    t_check.start()

    # 扫码生产进程
    t_produce = threading.Thread(
        target=produce_baozi_process,
        args=[
            master, ADR, ip_QR, port_QR
        ]
    ) 
    t_produce.start()

    # 扫码信息获取
    QR_scan_loop(scanner_name)

    t_check.join()
    t_produce.join()
