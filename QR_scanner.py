# -*- coding: utf-8 -*-
# -*- coding: utf-8 -*-
"""
***扫码枪读取***

Created on xxx xxx x xx:xx:xx xxxx

@author: xxx
"""

import evdev

# 按键转字符表 只列出了常用的字符
keymap = {
    'KEY_0': u'0',
    'KEY_1': u'1',
    'KEY_2': u'2',
    'KEY_3': u'3',
    'KEY_4': u'4',
    'KEY_5': u'5',
    'KEY_6': u'6',
    'KEY_7': u'7',
    'KEY_8': u'8',
    'KEY_9': u'9',
    'KEY_A': u'A',
    'KEY_B': u'B',
    'KEY_C': u'C',
    'KEY_D': u'D',
    'KEY_E': u'E',
    'KEY_F': u'F',
    'KEY_G': u'G',
    'KEY_H': u'H',
    'KEY_I': u'I',
    'KEY_J': u'J',
    'KEY_K': u'K',
    'KEY_L': u'L',
    'KEY_M': u'M',
    'KEY_N': u'N',
    'KEY_O': u'O',
    'KEY_P': u'P',
    'KEY_Q': u'Q',
    'KEY_R': u'R',
    'KEY_S': u'S',
    'KEY_T': u'T',
    'KEY_U': u'U',
    'KEY_V': u'V',
    'KEY_W': u'W',
    'KEY_X': u'X',
    'KEY_Y': u'Y',
    'KEY_Z': u'Z',
    'KEY_TAB': u'\t',
    'KEY_SPACE': u' ',
    'KEY_COMMA': u',',
    'KEY_SEMICOLON': u';',
    'KEY_EQUAL': u'=',
    'KEY_LEFTBRACE': u'[',
    'KEY_RIGHTBRACE': u']',    
    'KEY_MINUS': u'-',
    'KEY_APOSTROPHE': u'\'',
    'KEY_GRAVE': u'`',
    'KEY_DOT': u'.',
    'KEY_SLASH': u'/',
    'KEY_BACKSLASH': u'\\',
    'KEY_ENTER': u'\n',
}

def listen_device(device):
    ''' 监听扫码枪输入 '''
    '''
    输入说明：
        device：扫码枪设备
    '''
    buf = ''
    for event in device.read_loop():
        if event.type == evdev.ecodes.EV_KEY and event.value == 1:
            kv = evdev.events.KeyEvent(event)
            # 本次修改的地方, 把事件映射到字符表
            if (kv.scancode == evdev.ecodes.KEY_ENTER):
                print('读到输入: ', buf)
                with open('/tmp/baozi_qrcode', 'w') as f:
                    f.write(buf)
                # 清空 buffer
                buf = ''
            else:
                buf += keymap.get(kv.keycode, '')

def QR_scan_loop(scanner_name='HIDKeyBoard'):
    ''' 扫码枪读取 '''
    '''
    输入说明：
        scanner_name：扫码枪设备名
    '''
    devices = [evdev.InputDevice(path) for path in evdev.list_devices()]
    print('发现以下设备: ')
    for device in devices:
        print(device.path, device.name, device.phys)

    print('监听以下设备:')
    for device in devices:
        if scanner_name in device.name:
            print(device.path, device.name, device.phys)
            listen_device(device)


if __name__ == '__main__':
    QR_scan_loop()
