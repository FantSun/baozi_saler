# -*- coding: utf-8 -*-
# -*- coding: utf-8 -*-
"""
***PLC通信***

Created on xxx xxx x xx:xx:xx xxxx

@author: xxx
"""


import time
import serial
import serial.tools.list_ports
import modbus_tk
import modbus_tk.defines as cst
from modbus_tk import modbus_rtu

def modbus_write(master, ADR, addr, value):
    ''' 生产机器寄存器写入 '''
    '''
    输入说明：
        ADR：生产机器地址码
        addr：寄存器地址
        value：写入值
    返回值：
        从机返回信息（寄存器地址，写入值）
    '''

    info = None
    while(info == None):
        try:
            info =  master.execute(ADR, cst.WRITE_SINGLE_REGISTER, addr, output_value=value) # 信息写入
        except:
            print("写入失败，重新写入")
            continue
        if info[0] != addr or info[1] != value:
            info = None
            print("写入失败，重新写入")
            continue
    print(info)
    return info

def modbus_read(master, ADR, addr, num):
    ''' 生产机器寄存器读取 '''
    '''
    输入说明：
        ADR：生产机器地址码
        addr：寄存器起始地址
        num：寄存器数量
    返回值：
        从机回复信息（所请求读取的各个寄存器值，类型为list）
    '''
    info = None
    while(info == None):
        try:
            info =  master.execute(ADR, cst.READ_HOLDING_REGISTERS, addr, num) # 信息读取
        except:
            print("读取失败，重新读取")
            continue
        if len(info) != num:
            info = None
            print("读取失败，重新读取")
            continue
    print(info)
    return info
