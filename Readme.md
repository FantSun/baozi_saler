此项目为小汤包自动售卖机Nano端程序测试版本，程序在python3.6环境中实现，此文件为该版本简单说明。

1. 程序在Nvidia Jetson Nano套件中使用python3.6实现，具体python库需求可查看requirements.txt。推荐使用conda安装python虚拟环境，本地计算机可以使用anaconda，nano板中可使用miniforge代替anaconda，冯总处nano板已完成环境搭建（因有部分修改，暂时未完成，如需运行，请重新搭建），程序运行前请务必先运行

   `sh ~/.bashrc`

   `conda activate baozi-py36`

   `sudo chmod 777 /dev/ttyTHS1`

   其中，baozi-py36为冯总处nano板中可稳定运行程序的环境。若安装其他python包，请在该环境中使用

   `python -m pip install xxx`

   若自行搭建环境，可使用以下命令

   `conda activate -n xxx-env python=3.6`

   `python -m pip install -r requirements.txt`

   另外，注意若tensorflow安装遇到问题，请参考[安装说明](https://docs.nvidia.com/deeplearning/frameworks/install-tf-jetson-platform/index.html)，其中tensorflow安装命令为
   `python -m pip install --pre --extra-index-url https://developer.download.nvidia.com/compute/redist/jp/v42 tensorflow-gpu`

2. 主程序为main.py，运行方式为

   `python main.py`

   或直接运行bash文件run.sh（已包含环境载入和端口权限改写）

   该文件中包含地址码、ip地址、端口号等设置，运行前请确认这些设置。其中需要注意的是，在使用USB转接模拟测试时，modbus主机需要设置serial.Serial中parity参数为'N'，实机测试中则为'E'（也可通过其他测试设置进行测试，但通过该参数最为便利）。

3. 项目中各文件和函数的作用及输入输出已在注释中标明，部分函数沿用了之前的实现方式（不知道是哪位同学写的，此处就不引了），下面对各文件中内容进行简单说明：

   main.py：主进程

   progress_produce.py：扫码生产过程，如需修改寄存器地址码、写入值、预产方式等，请在此文件中修改

   progress_check.py：库存检查过程，如需修改寄存器地址码、短信内容等，请在此文件中修改

   counter.py：人群计数

   message.py：短信发送，待完成

   modbus.py：PLC通信

   push.py：包子推出，待完成

   QRcode.py：二维码信息文件读取

   QR_scanner.py：扫码枪输入读取及二维码信息文件存储

   其中，counter.py、message.py、modbus.py、push.py、QRcode.py、QR_scanner.py为子功能实现函数，确定实现的函数尽量避免修改，待完成的函数也应实现为不必频繁修改的形式；progress_produce.py、progress_check.py为流程实现函数，若修改PLC报文内容如寄存器地址、写入值等等，应在此处进行修改。

4. 项目中应当包含models、weights两个文件夹，其中为所用人脸识别模型参数及权值等，此上传版本未包含这些文件，可从之前CSRNet文件夹中将这两个文件夹复制至此即可。

5. 此说明为简略版本，最终版本应当包含对一些所用模型、硬件等的引用说明。
